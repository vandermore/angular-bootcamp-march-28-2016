( function( angular) {
	angular.module( 'appModule', [
		'employeesModule',
		'searchModule',
		'filtersModule'
	] );
})( window.angular );