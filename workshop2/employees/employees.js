( function( angular) {
	angular.module( 'employeesModule', [] )
		.service( 'employeeService', employeeService )
		.factory( 'shapesFactory', shapesFactory )
		.component( 'employeeDisplay', {
			templateUrl: 'employees/employeeDisplay.html',
			controller: EmployeeController
		})
		.component( 'employeeRenderer', {
			templateUrl: 'employees/employeeRenderer.html',
			bindings: {
				employee: '='
			}
		});

	function EmployeeController(
		employeeService,
		shapesFactory,
	  $filter
	) {
		var that = this;
		employeeService.getEmployees().then( employeesResult );
		employeeService.getEmployees().then( function( data ) {
			console.log( 'employees', $filter('checkmark')(data) );
		} );

		function employeesResult( data ) {
			that.employeeList = data;
		}

		shapesFactory.getShapes().then( shapesResults );
		function shapesResults( data ) {
			console.log( 'My Shapes', data );
			that.shapesData = data;
		}
	}

	function shapesFactory( $http ) {
		function getShapes() {
			return $http.get( '../../../demo-data/shapes.json' )
				.then( function( result ) {
					return result.data;
				});
		}

		return {
			getShapes: getShapes
		};
	}

	function employeeService( $http ) {
		var employeeCache = $http.get( '../../../demo-data/employees.json')
			.then( function( result ) {
				return result.data;
			});

		this.getEmployees = function() {
			return employeeCache;
		}
	}
})( window.angular );