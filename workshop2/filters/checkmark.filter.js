( function( angular) {
	angular.module( 'filtersModule', [] )
		.filter('checkmark', checkmark);

	function checkmark() {
		return function( input ) {
			if ( input ) {
				return '\u2713'
			}
			return input;
		}
	}
})( window.angular );