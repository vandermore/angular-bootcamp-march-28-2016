( function( angular ) {
	angular.module( 'abcModule', [] )
		.component( 'abcSearch', {
			templateUrl: 'abcSearch.html',
			controller: filteringExampleController,
			controllerAs: 'fec'
		} )
		.component( 'abcDisplay', {
			templateUrl: 'abcDisplay.html',
			bindings: {
				shapes: '=',
				query: '=searchTerms',
				hi: '&hello'
			}
		});

	function filteringExampleController() {
		this.mySayHiFunction = function() {
			console.log( 'hi!', this );
		};

		this.shapes = [
			{
				"name": "triangle",
				"sides": 3
			},
			{
				"name": "Square",
				"sides": 4
			},
			{
				"name": "Pentagon",
				"sides": 5
			}
		];
	}

})( window.angular);